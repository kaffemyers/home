#!/bin/bash

usage() {
	echo "Usage: $0 --source SOURCE [--api-key API_KEY] [--username USERNAME] [--password PASSWORD]"
	exit 1
}

add_or_update_credential() {
	local source=$1
	local key=$2
	local value=$3

	xmlstarlet sel -t -c "/configuration/packageSourceCredentials" "$nuget_config_path" &> /dev/null \
		|| xmlstarlet ed -L -s "/configuration" -t elem -n "packageSourceCredentials" -v "" "$nuget_config_path"

	xmlstarlet ed -L \
		-s "/configuration/packageSourceCredentials" -t elem -n "$source" -v "" \
		-s "/configuration/packageSourceCredentials/$source" -t elem -n "add" -v "" \
		-i "/configuration/packageSourceCredentials/$source/add[not(@key)]" -t attr -n "key" -v "$key" \
		-i "/configuration/packageSourceCredentials/$source/add[@key='$key']" -t attr -n "value" -v "$value" \
		"$nuget_config_path"
}

restore_backup() {
	echo "An error occurred. Restoring the backup..."
	cp "$backup_nuget_config_path" "$nuget_config_path"
	echo "Backup restored."
}

PARSED_ARGUMENTS=$(getopt -o '' --long source:,api-key:,username:,password: -- "$@") || usage
eval set -- "$PARSED_ARGUMENTS"

source=""
api_key=""
username=""
password=""

while :; do
	case "$1" in
		--source)
			source="$2"
			shift 2
			;;
		--api-key)
			api_key="$2"
			shift 2
			;;
		--username)
			username="$2"
			shift 2
			;;
		--password)
			password="$2"
			shift 2
			;;
		--)
			shift
			break
			;;
		*) usage ;;
	esac
done

[[ -n $source ]] || {
	echo "Error: --source is required."
	usage
}

nuget_config_path=$(dotnet nuget config paths)
backup_nuget_config_path="/tmp/$(basename "${nuget_config_path}").$(date --utc +%s).bak"
cp "$nuget_config_path" "$backup_nuget_config_path"

[[ -n $api_key ]] && add_or_update_credential "$source" "ApiKey" "$api_key"
[[ -n $username ]] && add_or_update_credential "$source" "Username" "$username"
[[ -n $password ]] && add_or_update_credential "$source" "ClearTextPassword" "$password"

echo "$nuget_config_path updated successfully."
