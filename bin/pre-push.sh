#!/bin/bash

readarray -t file_types < <(project-info-collector | jq -r '.file_types[]')

for file_type in "${file_types[@]}"; do
	case $file_type in
		yaml)
			yamllint .
			;;
		markdown)
			mdl .
			;;
		shell)
			shfmt --write --simplify .
			;;
		python)
			black -l 120 .
			isort .
			flake8 .
			;;
		plain)
			whitespace-police --clean
			;;
		json)
			json-lint
			;;
		docker)
			hadolint Dockerfile
			;;
		*)
			echo "No linter configured for file type: $file_type"
			;;
	esac
done
