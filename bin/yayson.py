#!/usr/bin/env python3
import json
import logging
import sys
from pathlib import Path
from typing import Optional, Union

import click
import git

logging.basicConfig(level=logging.WARNING, format="%(message)s")


def get_content(file_path: str) -> str:
    try:
        file_content = Path(file_path).read_text()
    except (FileNotFoundError, IsADirectoryError) as exception:
        logging.error(f"{exception.strerror}: {file_path}")
        sys.exit(1)
    return file_content


def line_message(file_path: str, line_number: int, column_number: int, message: str) -> str:
    return ":".join([file_path, str(line_number), str(column_number), f" {message}"])


def is_valid_json(file_path: str, format_file: bool, indent: Optional[int]) -> bool:
    indent_type = "space" if isinstance(indent, int) else "tab"
    file_content = get_content(file_path)

    try:
        loaded_json = json.loads(file_content)
        formatted_json = f"{json.dumps(loaded_json, indent=indent)}\n"

    except json.decoder.JSONDecodeError as exception:
        line_number, column_number = exception.lineno, exception.colno
        logging.error(line_message(file_path, line_number, column_number, exception.msg))
        return False

    if format_file:
        Path(file_path).write_text(formatted_json)
        return True

    original_lines = file_content.split("\n")
    formatted_lines = formatted_json.split("\n")

    if len(original_lines) != len(formatted_lines):
        line_number = len(original_lines)
        column_number = len(original_lines[-1])
        error_message = f"Line count incorrect, should be {len(formatted_lines)}"
        logging.error(line_message(file_path, line_number, column_number, error_message))
        return False

    indent_errors = []

    for line_number, (original_line, formatted_line) in enumerate(zip(original_lines, formatted_lines), start=1):
        original_indent = len(original_line) - len(original_line.lstrip())
        formatted_indent = len(formatted_line) - len(formatted_line.lstrip())

        if original_indent != formatted_indent:
            plural = "s" if formatted_indent > 1 else ""
            error_message = f"Indentation error, should be {formatted_indent} {indent_type}{plural}"
            indent_errors.append(line_message(file_path, line_number, original_indent, error_message))

    if indent_errors:
        for error in indent_errors:
            logging.warning(error)
        return False

    return True


def find_files_with_suffix(directory: str = ".", suffix: str = "json", no_git: bool = False) -> list[str]:
    path = Path(directory)
    files = []
    if not no_git:
        try:
            repo = git.Repo(path)
            tree = repo.tree()
            files = [
                str(path / item.path)
                for item in tree.traverse()
                if item.type == "blob" and item.path.endswith(f".{suffix}")
            ]
        except git.InvalidGitRepositoryError:
            pass

    if not files:
        files = list(path.rglob(f"*.{suffix}"))

    if path == Path("."):
        files = [f"./{file}" for file in files]
    else:
        files = [str(file) for file in files]

    return files


def int_or_tab(value: str) -> Optional[Union[int, str]]:
    if value in ("tab", "\t", "\\t"):
        return "\t"
    try:
        return int(value)
    except ValueError:
        raise click.BadParameter("Invalid value for --indent. Must be an integer or 'tab'.")


@click.command(
    help="A happy, basic linter for JSON files. Yay!",
    epilog="""
        If no files are provided on the command line, it will recursively search for JSON
        files in the current Git repository. If the script is not executed within a Git
        repository, it will recursively search for JSON files in the current directory.
        To specify a different Git repository or directory, use the --directory or -d flag
        followed by the directory path.
    """,
)
@click.option("--directory", "-d", default=".", help="Directory to search for JSON files (default: current directory)")
@click.option("--format", "-f", "format_file", is_flag=True, default=False, help="Format JSON files")
@click.option("--indent", "-i", type=int_or_tab, default="2", help="Indentation level for formatting (default: 2).")
@click.option("--no-git", "-n", is_flag=True, default=False, help="Ignore checking if it is a git repository.")
@click.argument("files", nargs=-1, type=click.Path(exists=True))
def lint_json(
    directory: str, format_file: bool, indent: Optional[Union[int, str]], no_git: bool, files: tuple[str]
) -> None:
    lint_files = files or find_files_with_suffix(directory)

    status = [is_valid_json(file, format_file, indent) for file in lint_files]
    exit_code = 0 if all(status) else 1
    sys.exit(exit_code)


if __name__ == "__main__":
    lint_json()
