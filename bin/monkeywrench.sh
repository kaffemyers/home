#!/bin/bash

###############
## Constants ##
###############

SCRIPT=$(basename "${BASH_SOURCE[0]}")

########################
## internal functions ##
########################

log() {
	local level=${MW_LOG_LEVEL:-info}
	printf "%s: " "${level^^}"
	printf "%s\n" "$@"
}

err_return() {
	local code="${MW_RETURN_CODE:-1}"
	MW_LOG_LEVEL=error log "$@" 1>&2
	return "$code"
}

##################
## global flags ##
##################

_global_flag_database() { ## string ## Cloud Spanner database name (optional. if not set, will use $SPANNER_DATABASE_ID value)
	echo
}

_global_flag_directory() { ## string ## Directory that schema file placed (required)
	echo
}

_global_flag_instance() { ## string ## Cloud Spanner instance name (optional. if not set, will use $SPANNER_INSTANCE_ID value)
	echo
}

_global_flag_project() { ## string ## GCP project id (optional. if not set, will use $SPANNER_PROJECT_ID or $GOOGLE_CLOUD_PROJECT value)
	echo
}

_global_flag_schema-file() { ## string ## Name of schema file (optional. if not set, will use default 'schema.sql' file name)
	echo
}

##################
## load command ##
##################

load() { ## none ## Load schema from server to file
	parse_functions
}

#####################
## migrate command ##
#####################

migrate() { ## sub-command ## Migrate database
	parse_functions
}

_migrate_cmds_create() {
	parse_functions
}

_migrate_cmds_up() {
	parse_functions
}

_migrate_cmds_version() {
	parse_functions
}

###################
## apply command ##
###################

apply() { ## none ## Apply DDL file to database
	parse_functions
}

_apply_flag_ddl() { ## string ## DDL file to be applied
	echo
}

_apply_flag_dml() { ## string ## DML file to be applied
	echo
}

_apply_flag_partitioned() { ## none ## Whether given DML should be executed as a Partitioned-DML or not
	echo
}

parse_functions() {
	local context="${FUNCNAME[1]}"
	[[ $context != main ]] || context=global

	awk -v context="$context" -v script_name="$SCRIPT" '
	/\S+\(\) *{ *## / {
		function_name = substr($1, 1, length($1) -2)
		the_rest = substr($0, index($0, "##") +2)
		sub(/^ */, "", the_rest)
		split(the_rest, a, / *## */)
		function_arg = a[1] != "none" ? a[1] : ""
		function_desc = a[2]

		match_context_flag = "^_" context "_flag"
		if(context == "global" && function_name !~ /_flag_/) {
			match_context_cmd = "^[a-z-]+$"
			sub_name = function_name
		} else {
			match_context_cmd = "^_" context "_cmds"
			split(function_name, function_name_split, "_")
			sub_name = function_name_split[4]
		}

		if(function_name ~ match_context_flag) {
			flag_name = "--" sub_name
			flags_name_char_count = length(flag_name) > flags_name_char_count ? length(flag_name) : flags_name_char_count
			flags_arg_char_count = length(function_arg) > flags_arg_char_count ? length(function_arg) : flags_arg_char_count
			flags_arg[flag_name] = function_arg
			flags_desc[flag_name] = function_desc
		} else if(function_name ~ match_context_cmd) {
			cmds_name_char_count = length(sub_name) > cmds_name_char_count ? length(sub_name) : cmds_name_char_count
			cmds_arg_char_count = length(function_arg) > cmds_arg_char_count ? length(function_arg) : cmds_arg_char_count
			cmds_arg[sub_name] = function_arg
			cmds_desc[sub_name] = function_desc
		}
	}
	END {
		printf "Usage:\n  %s", script_name
		if(context != "global") printf " %s", context

		if(length(cmds_arg) > 0) {
			print " [command]"
			print ""
			print "Available commands:"
			for(cmd in cmds_arg) {
				printf "  %-" cmds_name_char_count "s %-" cmds_arg_char_count "s  %s\n", cmd, cmds_arg[cmd], cmds_desc[cmd]
			}
		}

		if(length(flags_arg) > 0) {
			print ""
			print "Available commands:"
			for(flag in flags_arg) {
				printf "  %-" flags_name_char_count "s %-" flags_arg_char_count "s  %s\n", flag, flags_arg[flag], flags_desc[flag]
			}
		}
	}' "${BASH_SOURCE[0]}"
	echo -e '\n\n'"$context"
}

if (($#)); then
	"$@"
else
	parse_functions
fi
