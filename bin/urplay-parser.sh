#!/bin/bash
usage() {
	echo "Usage: $0 <url>"
	exit 1
}

initialize_db() {
	local db_file="$1"

	sqlite3 "$db_file" <<- EOF
		CREATE TABLE IF NOT EXISTS episodes (
			id INTEGER PRIMARY KEY,
			show_name TEXT,
			season TEXT,
			episode_url TEXT UNIQUE,
			timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
		);
	EOF
}

fetch_show_content() {
	local url="$1"

	curl --silent "$url"
}

extract_value() {
	local json="$1" key="$2"

	jq --raw-output --arg key "$key" '.[$key]' <<< "$json"
}

format_number() {
	local number

	read -r number
	printf '%02d' "$number"
}

format_string() {
	local string

	read -r string
	sed 's/[[:blank:][:punct:]]/./g; s/\.\+/./g' <<< "$string"
}

episode_exists() {
	local db_file="$1" episode_url="$2"

	sqlite3 "$db_file" "SELECT 1 FROM episodes WHERE episode_url = '$episode_url' LIMIT 1;"
}

download_episode() {
	local file_name="$1" episode_url="$2"

	svtplay-dl --force --output "$file_name" --output-format mkv "$episode_url"
	if check_mkv_validity "$file_name"; then
		echo "Download successful and file is valid."
	else
		echo "Download failed or file is invalid."
		rm -f "$file_name" # Remove the invalid file
		return 1
	fi
}

# this does not work as intended, so need to come up with something better
check_mkv_validity() {
	local file="$1.mkv"

	ffmpeg -v error -i "$file" -f null - 2>&1
}

insert_episode() {
	local db_file="$1" show_name="$2" season="$3" episode_url="$4"

	sqlite3 "$db_file" "
		INSERT INTO episodes (show_name, season, episode_url)
		VALUES ('$show_name', '$season', '$episode_url');
	"
}

extract_seasons() {
	local show_content="$1" domain="$2"
	local metadata

	metadata=$(pup 'div[class^="DropDown_dropDownListWrapper"] a json{}' <<< "$show_content")

	jq --compact-output --arg base_url "https://$domain" '
		.[]
		| {
				season_url: ($base_url + .href),
				season: (.text | match("[0-9]+$").string)
			}
	' <<< "$metadata"
}

default_season() {
	local url="$1"

	jq --null-input --compact-output --arg season_url "$url" '
		{
			season_url: $season_url,
			season: "1"
		}
	'
}

fetch_season_content() {
	local season_url="$1"
	curl --silent "$season_url"
}

extract_episodes() {
	local season_content="$1" domain="$2" show_name="$3" season="$4"
	local link_json meta_json combined_json

	link_json=$(
		pup 'div[data-testid="episodes-list"] a json{}' <<< "$season_content" \
			| jq '[.[] | .title = .children[0].text]'
	)
	meta_json=$(
		pup 'div[data-testid="episodes-list"] span json{}' <<< "$season_content" \
			| jq '[.[] | select(has("children"))]'
	)

	combined_json=$(jq --slurp 'transpose | map(add)' <(echo "$link_json") <(echo "$meta_json"))

	jq --compact-output --arg base_url "https://$domain" --arg show_name "$show_name" --arg season "$season" '
		.[]
		| {
				show_name: $show_name,
				season: $season,
				episode: (.text | match("[0-9]+$").string),
				title: .title,
				url: ($base_url + .href)
			}
	' <<< "$combined_json"
}

process_episode_json() {
	local episode_json="$1" db_file="$2" show_name="$3" season="$4"
	local episode_url season episode title dest_dir file_name

	episode_url=$(extract_value "$episode_json" "url")

	if [[ -z $(episode_exists "$db_file" "$episode_url") ]]; then
		episode=$(extract_value "$episode_json" "episode" | format_number)
		title=$(extract_value "$episode_json" "title" | format_string)

		dest_dir="${DESTDIR:-.}/$show_name/$season"
		mkdir --parents "$dest_dir"

		file_name="${dest_dir}/${show_name}.S${season}E${episode}.${title}"

		if download_episode "$file_name" "$episode_url"; then
			insert_episode "$db_file" "$show_name" "$season" "$episode_url"
		else
			echo "Failed to download or validate episode: $episode_url"
		fi
	fi
}

process_season_url() {
	local season_json="$1" domain="$2" show_name="$3" db_file="$4"
	local season_url season season_content

	season_url=$(extract_value "$season_json" "season_url")
	season=$(extract_value "$season_json" "season" | format_number)
	season_content=$(fetch_season_content "$season_url")

	readarray -t normalized_jsons < <(extract_episodes "$season_content" "$domain" "$show_name" "$season")

	for episode_json in "${normalized_jsons[@]}"; do
		process_episode_json "$episode_json" "$db_file" "$show_name" "$season"
	done
}

main() {
	(($#)) || usage

	set -euo pipefail

	local url="$1" domain="urplay.se" db_file="episodes.db"
	local show_content show_name

	initialize_db "$db_file"

	[[ $url == https://*"$domain"/* ]] || exit 1

	show_content=$(fetch_show_content "$url")
	show_name=$(pup 'h1 text{}' <<< "$show_content" | head -n 1 | xargs | format_string)

	readarray -t seasons_json < <(extract_seasons "$show_content" "$domain")
	((${#seasons_json[@]})) || seasons_json=("$(default_season "$url")")

	for season_json in "${seasons_json[@]}"; do
		process_season_url "$season_json" "$domain" "$show_name" "$db_file"
	done
}

main "$@"
