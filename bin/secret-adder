#!/bin/bash

check_auth() {
    if ! gcloud auth list --format=json | jq -e '.[0]'; then
        echo "Please run 'gcloud auth login --update-adc' to authenticate."
        exit 1
    fi
}

select_team() {
    if [[ -z $TEAM ]]; then
        if [[ -n $GCP_TEAM ]]; then
            TEAM=$GCP_TEAM
        else
            echo "Fetching available projects..."
            PROJECTS=$(gcloud projects list --format=json)
            TEAMS=$(jq -r '.[].labels.ownerteam' <<<"$PROJECTS" | sort -u | jq -R | jq -s)
            TEAM_COUNT=$(jq length <<<"$TEAMS")

            echo "Available teams:"
            for ((i = 1; i <= TEAM_COUNT; i++)); do
                TEAM_NAME=$(jq -r ".[$((i - 1))]" <<<"$TEAMS")
                echo "$i. $TEAM_NAME"
            done

            while true; do
                read -rp "Select a team by number (or type 'all' to list all projects): " TEAM_NUMBER
                if [[ $TEAM_NUMBER == "all" ]]; then
                    TEAM="all"
                    break
                elif [[ $TEAM_NUMBER =~ ^[0-9]+$ ]] && ((TEAM_NUMBER > 0 && TEAM_NUMBER <= TEAM_COUNT)); then
                    TEAM=$(jq -r ".[$((TEAM_NUMBER - 1))]" <<<"$TEAMS")
                    break
                else
                    echo "Invalid selection. Please enter a valid number or 'all'."
                fi
            done
        fi
    fi

    echo "Selected team: $TEAM"
}

select_project() {
    if [[ -z $PROJECT ]]; then
        if [[ $TEAM == "all" ]]; then
            FILTERED_PROJECTS="$PROJECTS"
        else
            FILTERED_PROJECTS=$(jq --arg TEAM "$TEAM" '[.[] | select(.labels.ownerteam == $TEAM)]' <<<"$PROJECTS")
        fi

        PROJECT_COUNT=$(jq length <<<"$FILTERED_PROJECTS")

        echo "Available projects:"
        for ((i = 1; i <= PROJECT_COUNT; i++)); do
            PROJECT_ID=$(jq -r ".[$((i - 1))].projectId" <<<"$FILTERED_PROJECTS")
            echo "$i. $PROJECT_ID"
        done

        while true; do
            read -rp "Select a project by number: " PROJECT_NUMBER
            if [[ $PROJECT_NUMBER =~ ^[0-9]+$ ]] && ((PROJECT_NUMBER > 0 && PROJECT_NUMBER <= PROJECT_COUNT)); then
                PROJECT=$(jq -r ".[$((PROJECT_NUMBER - 1))].projectId" <<<"$FILTERED_PROJECTS")
                break
            else
                echo "Invalid selection. Please enter a valid number."
            fi
        done
    fi

    gcloud config set project "$PROJECT"
}

select_secret() {
    if [[ -z $SECRET_ID ]]; then
        echo "Fetching available secrets..."
        SECRETS=$(gcloud secrets list --format=json)
        SECRET_COUNT=$(jq length <<<"$SECRETS")

        echo "Available secrets:"
        for ((i = 1; i <= SECRET_COUNT; i++)); do
            SECRET_NAME=$(jq -r ".[$((i - 1))].name" <<<"$SECRETS")
            echo "$i. $SECRET_NAME"
        done

        while true; do
            read -rp "Select a secret by number: " SECRET_NUMBER
            if [[ $SECRET_NUMBER =~ ^[0-9]+$ ]] && ((SECRET_NUMBER > 0 && SECRET_NUMBER <= SECRET_COUNT)); then
                SECRET_ID=$(jq -r ".[$((SECRET_NUMBER - 1))].name" <<<"$SECRETS")
                break
            else
                echo "Invalid selection. Please enter a valid number."
            fi
        done
    fi
}

update_secret() {
    read -rp "Enter the new secret value: " NEW_SECRET_VALUE
    echo -n "$NEW_SECRET_VALUE" | gcloud secrets versions add "$SECRET_ID" --data-file=-
    echo "Secret updated successfully."
}

while (($#)); do
    case $1 in
    --team)
        TEAM="$2"
        shift
        ;;
    --project)
        PROJECT="$2"
        shift
        ;;
    --secret-id)
        SECRET_ID="$2"
        shift
        ;;
    *)
        echo "Unknown parameter passed: $1"
        exit 1
        ;;
    esac
    shift
done

check_auth
select_team
select_project
select_secret
update_secret
