#!/bin/bash
until awk '{ printf $0 }' ~/.gcloud | gcloud projects list > /dev/null 2>&1; do
	echo "gcloud: login required, opening browser"
	gcloud auth login --update-adc > /dev/null 2>&1
	sleep 2
done &
