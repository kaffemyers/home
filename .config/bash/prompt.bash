#!/bin/bash

[[ "$TERM" =~ xterm-color|.*-256color ]] && color_prompt=yes

[[ -z "${debian_chroot:-}" ]] && [[ -r /etc/debian_chroot ]] && debian_chroot=$(cat /etc/debian_chroot)

__parse_git_status() {
	case $1 in
		branch) git branch --show-current 2>/dev/null | sed 's/.*/ (&)/' ;;
		color) git status --porcelain 2>/dev/null | awk -v RS='\\?\\? ' 'END{print NR==0?92:(NR>1?91:93)}' ;;
	esac
}

PS1='${debian_chroot:+($debian_chroot)}\u@\h:'
if [[ "$color_prompt" == yes ]]; then
	PS1+='\[\e[32m\]\w\[\e[$(__parse_git_status color)m\]$(__parse_git_status branch)\[\e[00m\]$ '
else
	PS1+='\w$(__parse_git_status branch)$ '
fi
export PS1

if [[ -x /usr/bin/dircolors ]]; then
	if [[ -r ~/.dircolors ]]; then
			eval "$(dircolors -b ~/.dircolors)"
	else
			eval "$(dircolors -b)"
	fi

	alias ls='ls --color=auto'
fi
