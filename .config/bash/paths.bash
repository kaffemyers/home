PATH+=$(
	f() { for dir in "$@"; do [[ -d "$dir" ]] && printf ":%s" "$dir"; done ;}
	f "$HOME/bin" "$HOME/.cargo/bin" "$HOME/.local/bin" "/home/kaffe/.dotnet"
)

export PATH
