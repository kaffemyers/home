#!/bin/bash
g() { git "$@" ;}

k() { kubectl "$@" ;}

c() {
	local scale=8 help options

	read -rd '' help <<-EOF
		Usage: c [OPTIONS] EXPRESSION
		Evaluate mathematical EXPRESSION with optional scale.

		Options:
		  -s, --scale SCALE    Set the scale for precision (default: 8)
		  -h, --help           Display this help message
	EOF

	options=$(getopt -o s:h --long scale:,help -n 'c' -- "$@") || { echo "Failed parsing options." >&2; return 1 ;}

	eval set -- "$options"

	while true; do
		case "$1" in
			-s | --scale ) scale="$2"     ; shift 2  ;;
			-h | --help  ) echo "$help"   ; return 0 ;;
			--           ) shift          ; break    ;;
			*) echo "Internal error!" >&2 ; return 1 ;;
		esac
	done

	(( $# == 0 )) && { echo "Expression is required. Use -h or --help for usage information."; return 1; }

	<<<"scale=$scale; ($*)/1" bc | sed -E '/\./s/\.?0+$//; /^[\.-]/s/^(.*)(\.)/\10\2/'
}

? () {
	local cmd="$1"
	shift

	hash "$cmd" 2> /dev/null || {
		echo "Command $cmd does not exist on this system, bruh..." 1>&2
		return 1
	}

	local methods=("--help" "help" "-h" "man")

	for method in "${methods[@]}"; do
		case $method in
			--help | help | -h) help_section=$($cmd "$method") && break ;;
			man) help_section=$($method "$cmd") && break ;;
		esac
	done

	if (( $# )); then
		local keyword=$1
		if [[ ${keyword:0:2} == -- ]]; then
			keyword="$keyword(=|[[:space:]])"
		else
			keyword="^[[:space:]]*$keyword"
		fi

		awk -v k="$keyword" '
			/^([[:alnum:]].*|[[:space:]]*(-.*)?)$/ && f { exit }
			$0~k{f=1}
			f
		' <<< "$help_section"

	else
		echo "$help_section"
	fi
}
