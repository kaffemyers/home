#!/usr/bin/env bash

apply_xrandr_settings() {
	local monitors=("$@")

	xrandr-profile exists || xrandr-profile save

	monitors_regexp="^($(
		IFS="|"
		echo "${monitors[*]}"
	)$)"
	read -ra monitors_off < <(printf " --output %s --off" "${monitors[@]}")

	for ((i = 0; i < 3; i++)); do # I don't wanna live forever
		xrandr-profile load
		online_monitors=$(bspc query -M --names | grep --extended-regexp --count "$monitors_regexp")

		if (($# == online_monitors)); then
			return
		else
			xrandr "${monitors_off[@]}"
		fi
	done

	xrandr-profile load
}

move_nodes_to_next_local_desktop() {
	local desktops=("$@")

	for desktop in "${desktops[@]}"; do

		while IFS= read -r node; do
			bspc node "$node" -d next.local
		done < <(bspc query -N -d "$desktop")

		bspc desktop "$desktop" -n Desktop

	done
}

move_desktops_to_monitor() {
	local monitor=$1 desktops=("${@:2}")
	for desktop in "${desktops[@]}"; do
		bspc desktop "$desktop" --to-monitor "$monitor"
	done
}

update_desktop_names() {
	local prior_desktops first_monitor=$1 desktops=("${@:2}")
	readarray -t prior_desktops < <(bspc query -D --names)

	for i in "${!prior_desktops[@]}"; do
		prior_desktop=${prior_desktops[i]}
		new_desktop=${desktops[i]}

		if [[ -n $new_desktop ]]; then
			bspc desktop "$prior_desktop" -n "$new_desktop"
			unset "desktops[$i]"
		else
			move_nodes_to_next_local_desktop "$prior_desktop"
		fi
	done

	for desktop in "${desktops[@]}"; do
		bspc monitor "$first_monitor" -a "$desktop"
	done
}

handle_disconnected_monitors() {
	local desktops bspwm_monitors connected_monitors=("$@")
	readarray -t bspwm_monitors < <(bspc query -M --names)

	for bspwm_monitor in "${bspwm_monitors[@]}"; do
		# shellcheck disable=SC2076
		if ! [[ " ${connected_monitors[*]} " =~ " $bspwm_monitor " ]]; then

			readarray -t desktops < <(bspc query -D -m "$bspwm_monitor")

			bspc monitor "$bspwm_monitor" -a Desktop
			move_desktops_to_monitor "${connected_monitors[0]}" "${desktops[@]}"
			bspc monitor "$bspwm_monitor" --remove
		fi
	done
}

main() {
	desktops=("$@")
	read -ra monitors < <(xrandr | awk '/ connected / {printf "%s ", $1}')

	number_of_desktops=${#desktops[@]}
	number_of_monitors=${#monitors[@]}

	apply_xrandr_settings "${monitors[@]}"


	desktop_per_monitor=$((number_of_desktops / number_of_monitors))
	desktop_count_per_monitor=$((desktop_per_monitor + (number_of_desktops % number_of_monitors)))
	starting_desktop_index=0

	update_desktop_names "${monitors[0]}" "${desktops[@]}"
	handle_disconnected_monitors "${monitors[@]}"

	for monitor in "${monitors[@]}"; do
		move_desktops_to_monitor "$monitor" "${desktops[@]:starting_desktop_index:desktop_count_per_monitor}"
		bspc monitor "$monitor" --reorder-desktops "${desktops[@]:starting_desktop_index:desktop_count_per_monitor}"

		starting_desktop_index=$((starting_desktop_index + desktop_count_per_monitor))
		desktop_count_per_monitor=$desktop_per_monitor
	done

	for ((i = 0; i < 20; i++)); do # I don't wanna live forever
		bspc query -D --names | grep --quiet "^Desktop$" || break
		bspc desktop Desktop --remove
	done

	bspc wm -O "${monitors[@]}"
}

bspwmrc_reload=$1
read -ra desktops <<< "${*:2}"

main "${desktops[@]}"

((bspwmrc_reload)) || main "${desktops[@]}"
