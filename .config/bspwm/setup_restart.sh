#!/usr/bin/env bash

service_file="$HOME/.config/systemd/user/bspwm-reload.service"
mkdir --verbose --parents "$(dirname "$service_file")"

cat <<- EOF | tee "$service_file"
	[Unit]
	Description=Reload BSPWM

	[Service]
	Type=oneshot
	ExecStart=/bin/bash -c "bspc wm -r"
	StandardOutput=journal
EOF

cat <<- EOF | sudo tee /etc/udev/rules.d/99-reload-monitor.rules
	ACTION=="change", SUBSYSTEM=="drm", RUN+="/bin/su $USER --command='systemctl --user start bspwm-reload.service'"
EOF
