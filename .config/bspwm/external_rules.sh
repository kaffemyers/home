#!/usr/bin/env bash
wid=$1
class=$2
#instance=$3
#consequences=$4
unset output_arr

silly_logger() {
	local log=/tmp/external_rules.log IFS="|"
	echo "$*" >> $log
	(($(stat --format %s "$log") < 1024)) || sed -i '1,2d' "$log"
}

silly_logger "$@"


kill_wid() {
	(
		sleep 0.3
		bspc node "$wid" -c
	) &
	disown
}

finger() {
	local current_active_wid target=$1

	(
		sleep 0.6
		current_active_wid=$(xdo id)
		for id in $(bspc query -N); do
			xprop -id "$id" 2>&1 \
				| awk -v id="$id" -v target="$target" '$0 ~ target { print id; exit }' \
				| xargs xdo activate
		done

		# Tab
		xdo key_press -k 23
		xdo key_release -k 23
		sleep 0.1

		# Shift + Tab
		xdo key_press -k 50
		xdo key_press -k 23
		xdo key_release -k 23
		xdo key_release -k 50

		xdo activate "$current_active_wid"
	) &
	disown
}

output() { output_arr+=("$@"); }
focus() { output focus="$1"; }
float() { output state=floating; }
desktop() { output desktop="$1" ;}

[[ -n $class ]] || class=Empty

case $class in
	Yad)
		focus on
		float
		;;
	Teams | update-notifier)
		focus off
		float
		desktop 3
		kill_wid
		;;
	'Idle timer expired')
		focus off
		float
		desktop 3
		kill_wid
		finger "Profile 1"
		;;
	Microsoft​\ Edge | 'C:\WINDOWS\system32\cmd.exe' | Wfica* | Empty)
		focus off
		float
		;;
esac

if ((${#output_arr[*]})); then
	echo "${output_arr[*]}"
fi
