local global = vim.g

global.mapleader = " "
global.maplocalleader = " "

require "config.vim"
require "plugins"
