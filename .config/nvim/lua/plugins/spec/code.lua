return {
   -- {
   --     "zbirenbaum/copilot.lua",
   --     cmd = "Copilot",
   --     event = "InsertEnter",
   --     config = function()
   --         require('copilot').setup({
   --             panel = {
   --                 enabled = false,
   --             },
   --             suggestion = {
   --                 enabled = false,
   --             },
   --             filetypes = {
   --                 yaml = false,
   --                 markdown = false,
   --                 help = false,
   --                 gitcommit = false,
   --                 gitrebase = false,
   --                 hgcommit = false,
   --                 svn = false,
   --                 cvs = false,
   --                 ["."] = false,
   --             },
   --             copilot_node_command = 'node', -- Node.js version must be > 18.x
   --             server_opts_overrides = {},
   --         })
   --     end,
   -- },
    -- LSP/DAP/Lint Manager
    {
        "williamboman/mason.nvim",
        cmd = "Mason",
        build = ":MasonUpdate",
        opts = {
            ui = {
                border = "rounded",
            }
        }
    },
    -- DAP
    {
        "mfussenegger/nvim-dap",
        dependencies = {
            "williamboman/mason.nvim",
            "jay-babu/mason-nvim-dap.nvim",
            "nvim-neotest/nvim-nio",
            "rcarriga/nvim-dap-ui",
            "theHamsta/nvim-dap-virtual-text"
        },
        event = "LspAttach",
        config = function()
            local icons = require("config.icons")

            require("nvim-dap-virtual-text").setup({})
            require("dapui").setup({})
            require('mason-nvim-dap').setup({
                ensure_installed = {},
                handlers = {}, -- sets up dap in the predefined manner
            })
            vim.api.nvim_set_hl(0, "DapStoppedLine", { default = true, link = "Visual" })

            for name, sign in pairs(icons.debugger) do
                sign = type(sign) == "table" and sign or { sign }
                vim.fn.sign_define(
                    "Dap" .. name,
                    { text = sign[1], texthl = sign[2] or "DiagnosticInfo", linehl = sign[3], numhl = sign[3] }
                )
            end
        end
    },
    -- LSP
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "hrsh7th/cmp-nvim-lsp",
            "nvimtools/none-ls.nvim",
            "jay-babu/mason-null-ls.nvim",
            --"Decodetalkers/csharpls-extended-lsp.nvim",
            "Hoffs/omnisharp-extended-lsp.nvim",
            "simrat39/rust-tools.nvim",
            "folke/neodev.nvim",
            "https://git.sr.ht/~whynothugo/lsp_lines.nvim"
        },
        event = { "BufReadPre", "BufNewFile" },
        config = function()
            require("neodev").setup({})
            require("lsp_lines").setup({})

            local icons = require("config.icons")
            local lspconfig = require("lspconfig")
            local mason_lspconfig = require("mason-lspconfig")
            local rust_tools = require("rust-tools")

            local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

            local on_attach = function(_, _) -- client, bufnr
                local keybinds = require("config.keybinds")

                require("legendary").keymaps(keybinds.lsp_on_attach())
            end

            local on_attach_omnisharp = function(_, _)
                local keybinds = require("config.keybinds")
                local legendary = require("legendary")

                legendary.keymaps(keybinds.lsp_on_attach())
                legendary.keymaps(keybinds.lsp_on_attach_omnisharp_override())
            end

            mason_lspconfig.setup({
                ensure_installed = { "lua_ls" },

                automatic_installation = { exclude = { "tsserver" } }
            })
            mason_lspconfig.setup_handlers {
                function(server_name)
                    lspconfig[server_name].setup {
                        on_attach = on_attach,
                        capabilities = capabilities
                    }
                end,

                ["tsserver"] = function()
                    lspconfig.tsserver.setup {
                        on_attach = on_attach,
                        capabilities = capabilities,
                        cmd = {
                            "npx", "-p", "typescript-language-server", "typescript-language-server", "--stdio"
                        }
                    }
                end,

                ["omnisharp"] = function()
                    lspconfig.omnisharp.setup {
                        on_attach = on_attach_omnisharp,
                        capabilities = capabilities,
                        handlers = {
                            ["textDocument/definition"] = require('omnisharp_extended').definition_handler,
                            ["textDocument/references"] = require('omnisharp_extended').references_handler,
                            ["textDocument/implementation"] = require('omnisharp_extended').implementation_handler,
                        },
                        --cmd = { "csharp-ls" }
                    }
                end,

                ["lua_ls"] = function()
                    lspconfig.lua_ls.setup {
                        on_attach = on_attach,
                        capabilities = capabilities,
                        settings = {
                            Lua = {
                                hint = {
                                    enable = true
                                },
                                runtime = {
                                    version = "LuaJIT",
                                },
                                diagnostics = {
                                    globals = { "vim" },
                                },
                                workspace = {
                                    -- make the server aware of Neovim runtime files
                                    library = {
                                        [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                                        [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true
                                    },
                                    checkThirdParty = false
                                },
                                telemetry = { enable = false },
                            },
                        }
                    }
                end,

                ["rust_analyzer"] = function()
                    rust_tools.setup({
                        tools = {
                            -- executor = require("rust-tools.executors").quickfix,
                            inlay_hints = {
                                auto = true,
                                parameter_hints_prefix = "<-",
                                other_hints_prefix = "->",
                            },
                            server = {
                                standalone = false,
                            },
                            dap = (function()
                                local install_root_dir = vim.fn.stdpath "data" .. "/mason"
                                local extension_path = install_root_dir .. "/packages/codelldb/extension/"
                                local codelldb_path = extension_path .. "adapter/codelldb"
                                local liblldb_path = extension_path .. "lldb/lib/liblldb.so"

                                return {
                                    adapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_path, liblldb_path)
                                }
                            end)(),
                        },
                        server = {
                            on_attach = on_attach,
                        },
                        capabilities = capabilities
                    })
                end

            }

            local null_ls = require("null-ls")
            null_ls.setup({
                sources = {
                    -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins
                    null_ls.builtins.formatting.black,
                    null_ls.builtins.formatting.shfmt,
                    null_ls.builtins.diagnostics.hadolint,
                }
            })

            for type, icon in pairs(icons.diagnostics) do
                local hl = "DiagnosticSign" .. type
                vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
            end

            vim.diagnostic.config({
                signs = true,
                update_in_insert = false,
                underline = true,
                severity_sort = true,
                virtual_text = { severity = { min = vim.diagnostic.severity.ERROR } },
                virtual_lines = { only_current_line = true }
            })

            -- vim.diagnostic.config({
            --  virtual_text = {severity = {min = vim.diagnostic.severity.WARN}},
            --  signs = {severity = {min = vim.diagnostic.severity.WARN}},
            --  underline = {severity = {min = vim.diagnostic.severity.WARN}},
            --})
        end
    },
    {
        "aznhe21/actions-preview.nvim",
        event = "LspAttach",
        config = function()
            require("actions-preview").setup({ backend = "telescope" })
        end
    },
    --{ 'kosayoda/nvim-lightbulb' },
    {
        "nvimdev/lspsaga.nvim",
        dependencies = {
            { "nvim-tree/nvim-web-devicons" },
            { "nvim-treesitter/nvim-treesitter" }
        },
        event = "LspAttach",
        config = function()
            local icons = require("config.icons")

            --vim.highlight.create()
            vim.api.nvim_set_hl(0, "SagaLightBulb", { fg = "#e0af68" })
            require("lspsaga").setup({
                ui = {
                    expand = icons.lsp.Expand,
                    collapse = icons.lsp.Collapse,
                    code_action = icons.lsp.CodeAction,
                    actionfix = icons.lsp.ActionFix,
                }
            })
        end,
    },
    {
        "stevearc/aerial.nvim",
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
            "nvim-tree/nvim-web-devicons"
        },
        event = "LspAttach",
        opts = {
            on_attach = require("config.keybinds").aerial()
        }
    },
    -- CMP
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-nvim-lsp-document-symbol",
            "hrsh7th/cmp-nvim-lsp-signature-help",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-nvim-lua",
            "lukas-reineke/cmp-under-comparator",
            "onsails/lspkind.nvim",
            {
                "L3MON4D3/LuaSnip",
                dependencies = { "rafamadriz/friendly-snippets" }
            },
            "saadparwaiz1/cmp_luasnip",
            "doxnit/cmp-luasnip-choice",
            "zbirenbaum/copilot-cmp",
        },
        event = "InsertEnter",
        config = function()
            local cmp = require("cmp")
            local lspkind = require("lspkind")
            local luasnip = require("luasnip")

            luasnip.setup({
                region_check_events = "InsertEnter",
                delete_check_events = "InsertEnter"
            })
            require("luasnip.loaders.from_vscode").lazy_load()
            require("cmp_luasnip_choice").setup({ auto_open = true })
            require("copilot_cmp").setup()

            cmp.setup({
                snippet = {
                    expand = function(args)
                        luasnip.lsp_expand(args.body)
                    end
                },
                window = {
                    completion = cmp.config.window.bordered(),
                    documentation = cmp.config.window.bordered()
                },
                mapping = require("config.keybinds").completion(),
                sources = cmp.config.sources({
                    { name = "nvim_lsp_signature_help" },
                    { name = "nvim_lsp" },
                    { name = "luasnip" },
                    { name = "luasnip_choice" },
                    { name = "buffer" },
                    { name = "nvim_lua" },
                    { name = "copilot" }
                }),
                sorting = {
                    comparators = {
                        cmp.config.compare.offset, cmp.config.compare.exact,
                        cmp.config.compare.score,
                        require "cmp-under-comparator".under,
                        cmp.config.compare.kind, cmp.config.compare.sort_text,
                        cmp.config.compare.length, cmp.config.compare.order
                    }
                },
                formatting = {
                    format = lspkind.cmp_format({
                        mode = "symbol_text",
                        maxwidth = 50,
                        ellipsis_char = "...",
                        symbol_map = { Copilot = "" }
                    })
                }
            })

            cmp.setup.cmdline({ "/", "?" }, {
                mapping = cmp.mapping.preset.cmdline(),
                sources = cmp.config.sources(
                    {
                        { name = "nvim_lsp_document_symbol" }
                    },
                    {
                        { name = "buffer" }
                    }
                )
            })

            cmp.setup.cmdline(":", {
                mapping = cmp.mapping.preset.cmdline(),
                sources = cmp.config.sources(
                    {
                        { name = "path" }
                    },
                    {
                        { name = "cmdline" }
                    }
                )
            })
        end
    }
}
