return {
    {
        "KaffeMyers/blackcoffee.nvim",
        priority = 1000,
        config = function()
           vim.cmd([[colorscheme blackcoffee]])
        end
    },
}
