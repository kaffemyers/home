return {
    {
        "nvim-lualine/lualine.nvim",
        event = "VeryLazy",
        opts = {
            theme = "auto",
        },
    },
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        config = true
    },
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            {
                "nvim-lua/plenary.nvim",
            },
            {
                "nvim-telescope/telescope-file-browser.nvim",
            },
            {
                "nvim-telescope/telescope-fzf-native.nvim",
                build = "make"
            },
            {
                "stevearc/aerial.nvim",
                dependencies = {
                    "nvim-treesitter/nvim-treesitter",
                    "nvim-tree/nvim-web-devicons",
                },
            },
        },
        cmd = "Telescope",
        config = function()
            local telescope = require "telescope"

            telescope.setup({
                extensions = {
                    file_browser = {
                        respect_gitignore = true,
                        hidden = {
                            file_browser = true,
                            folder_browser = true
                        },
                    }
                },
                pickers = {
                    find_files = {
                        no_ignore = true,
                        no_ignore_parent = true,
                        hidden = true
                    },
                    live_grep = {
                        additional_args = { "--no-ignore", "--hidden" }
                    },
                    grep_string = {
                        additional_args = { "--no-ignore", "--hidden" }
                    }
                }
            })

            telescope.load_extension("fzf")
            telescope.load_extension("file_browser")
            telescope.load_extension("aerial")
        end
    },
    --{
    --    "folke/noice.nvim",
    --    dependencies = {
    --        "MunifTanjim/nui.nvim",
    --        "rcarriga/nvim-notify",
    --    },
    --    event = "VeryLazy",
    --    opts = {
    --        cmdline = {
    --            enabled = true,
    --        },
    --        popup_menu = {
    --            enabled = false,
    --        },
    --        presets = {
    --            long_message_to_split = true
    --        },
    --        lsp = {
    --            -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
    --            override = {
    --                ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
    --                ["vim.lsp.util.stylize_markdown"] = true,
    --                ["cmp.entry.get_documentation"] = true,
    --            },
    --        },
    --    },
    --},
    {
        "akinsho/toggleterm.nvim",
        cmd = "ToggleTerm",
        opts = {
            start_in_insert = true,
            direction = "float",
        }
    },
}
