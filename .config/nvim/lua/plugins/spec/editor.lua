local icons = require("config.icons")

return {
    {
        "mrjones2014/legendary.nvim",
        dependencies = {
            'stevearc/dressing.nvim',
        },
        config = function(_) -- opts
            require("legendary").setup({
                keymaps = require("config.keybinds").general_keymaps(),
            })
        end
    },
    {
        "ggandor/leap.nvim",
        event = { "BufReadPost", "BufNewFile" },
        dependencies = { "tpope/vim-repeat" },
        opts = {
        }
    },
    {
        "ggandor/flit.nvim",
        event = { "BufReadPost", "BufNewFile" },
        dependencies = {
            "ggandor/leap.nvim",
            "tpope/vim-repeat",
        },
        config = function()
            require("flit").setup({
                labeled_modes = "nv"
            })
        end
    },
    {
        "nvim-treesitter/nvim-treesitter",
        dependencies = {
            "nvim-treesitter/nvim-treesitter-textobjects",
        },
        build = ":TSUpdate",
        event = { "BufReadPost", "BufNewFile" },
        config = function()
            require "nvim-treesitter.configs".setup({
                highlight = {
                    enable = true
                },
                indent = {
                    enable = true
                },
                ensure_installed = {
                    "lua",
                    "markdown",
                    "markdown_inline",
                },
                auto_install = true,
                textobjects = {
                    select = {
                        enable = true,
                        lookahead = true,
                        keymaps = {
                            ["cac"] = { query = "@class.outer", desc = "Select outer part of a class" },
                            ["cic"] = { query = "@class.inner", desc = "Select inner part of a class" },
                            ["caf"] = { query = "@function.outer", desc = "Select outer part of a function" },
                            ["cif"] = { query = "@function.inner", desc = "Select inner part of a function" },
                            ["cab"] = { query = "@block.outer", desc = "Select outer part of a block" },
                            ["cib"] = { query = "@block.inner", desc = "Select inner part of a block" },
                            ["cap"] = { query = "@parameter.outer", desc = "Select outer part of a parameter" },
                            ["cip"] = { query = "@parameter.inner", desc = "Select inner part of a parameter" },
                        },
                        -- You can choose the select mode (default is charwise 'v')
                        --
                        -- Can also be a function which gets passed a table with the keys
                        -- * query_string: eg '@function.inner'
                        -- * method: eg 'v' or 'o'
                        -- and should return the mode ('v', 'V', or '<c-v>') or a table
                        -- mapping query_strings to modes.
                        selection_modes = {
                            ['@parameter.outer'] = 'v', -- charwise
                            ['@function.outer'] = 'V', -- linewise
                            ['@class.outer'] = '<c-v>', -- blockwise
                        },
                        -- If you set this to `true` (default is `false`) then any textobject is
                        -- extended to include preceding or succeeding whitespace. Succeeding
                        -- whitespace has priority in order to act similarly to eg the built-in
                        -- `ap`.
                        --
                        -- Can also be a function which gets passed a table with the keys
                        -- * query_string: eg '@function.inner'
                        -- * selection_mode: eg 'v'
                        -- and should return true or false
                        include_surrounding_whitespace = false,
                    },
                },
            })
        end
    },
    {
        "nvim-treesitter/nvim-treesitter-context",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        event = { "BufReadPost", "BufNewFile" },
        opts = {
            max_lines = 3
        }
    },
    {
        "lukas-reineke/indent-blankline.nvim",
        dependencies = {
            "nvim-treesitter/nvim-treesitter"
        },
        event = { "BufReadPost", "BufNewFile" },
        main = "ibl",
        opts = {
            --use_treesitter = true,
            --use_treesitter_scope = true,
            --show_current_context = true,
            --show_current_context_start = false,
        }
    },
    {
        "laytan/cloak.nvim",
        -- event = { "BufReadPre" },
        config = function()
            require("cloak").setup(
                {
                    enabled = true,
                    cloak_character = require("config.icons").misc.SecretMask,
                    highlight_group = "Comment",
                    cloak_length = nil,
                    cloak_telescope = true,
                    patterns = {
                        {
                            file_pattern = { "*.env" },
                            cloak_pattern = "=.+"
                        },
                    },
                }
            )
        end
    },
    {
        "lewis6991/gitsigns.nvim",
        event = { "BufReadPre", "BufNewFile" },
        opts = {
            debug_mode=true,
            signs = {
                add          = { text = icons.git.Added },
                change       = { text = icons.git.Modified },
                delete       = { text = icons.git.Removed },
                topdelete    = { text = icons.git.Removed },
                changedelete = { text = icons.git.Modified },
                untracked    = { text = icons.git.Untracked },
            },
        },
    },
    {
        "m4xshen/autoclose.nvim",
        event = "InsertEnter"
    },
    {
        "romainl/vim-cool",
        event = { "CursorMoved", "InsertEnter" }
    },
    {
        "folke/twilight.nvim",
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
        },
        cmd = { "Twilight", "TwilightEnable", "TwilightDisable" },
        opts = {
            expand = {
                "function_item",
                "method_item",
                "function_definition",
                "method_definition",
                "table_constructor",
                "function_declaration",
            },
            context = 10
        },

    },
    {
        "folke/zen-mode.nvim",
        dependencies = {
            "folke/twilight.nvim"
        },
        cmd = "ZenMode",
        opts = {
            window = {
                options = {
                    number = true,
                    relativenumber = true,
                }
            },
            plugins = {
                twilight = {
                    enabled = true
                },
                gitsigns = {
                    enabled = false
                }
            }
        }
    }
    -- sindrets/diffview.nvim
    -- RRethy/vim-illuminate
}
