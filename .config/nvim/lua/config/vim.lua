local options = vim.o
local command = vim.cmd

options.guifont = "DankMono Nerd Font:h14"
options.mouse = ""
options.clipboard = "unnamedplus"

options.updatetime = 100
options.scrolloff = 999

options.tabstop = 4
options.colorcolumn = "120"
options.shiftwidth = 4
options.expandtab = true
options.signcolumn = "yes"

options.number = true
options.relativenumber = true

options.listchars = "eol:↴,tab:>·,trail:~,extends:>,precedes:<,space:·"
options.list = true

options.background = "dark"
options.termguicolors = true

options.completeopt = "menuone,noselect"

--options.swap = false
options.swapfile = false
options.autoread = true

command("match errorMsg /\\s\\+$/")
command("set wrap linebreak")

local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

local line_toggle = augroup("line_toggle", { clear = true })
autocmd(
    "InsertLeave",
    {
        callback = function()
            vim.wo.relativenumber = true
        end,
        group = line_toggle
    }
)
autocmd(
    "InsertEnter",
    {
        callback = function()
            vim.wo.relativenumber = false
        end,
        group = line_toggle
    }
)
autocmd(
    { "BufEnter", "CursorHold", "CursorHoldI", "FocusGained" },
    {
        callback = function()
            if vim.fn.mode() ~= "c" then
                vim.api.nvim_command("checktime")
            end
        end,
    }
)
autocmd(
    "BufEnter",
    {
        callback = function()
            if vim.bo.buftype ~= "" then
                return
            end

            local nvim_config_path = vim.env.HOME .. "/.config/nvim"
            local new_cwd_path = vim.fn.fnamemodify(
                vim.fn.finddir(".git", ".;"),
                ":h"
            ) or vim.env.HOME

            if new_cwd_path == vim.fn.getcwd() then
                return
            end

            if vim.fn.expand("%:p:h"):find("^" .. nvim_config_path) ~= nil then
                new_cwd_path = nvim_config_path
            end

            if new_cwd_path ~= "." then
                print("Changing cwd: ", new_cwd_path)
                vim.api.nvim_set_current_dir(new_cwd_path)
            end
        end
    }
)

autocmd("BufWritePre", {
  pattern = "*.md",
  callback = function()
    local bufnr = vim.api.nvim_get_current_buf()
    local lines = vim.api.nvim_buf_get_lines(bufnr, 0, -1, false)
    local frontmatter_start, frontmatter_end = nil, nil
    local date = os.date("!%Y-%m-%dT%H:%M:%S+00:00")

    -- Identify the start and end of the frontmatter
    for i, line in ipairs(lines) do
      if line:match("^%+%+%+") then
        if not frontmatter_start then
          frontmatter_start = i
        else
          frontmatter_end = i
          break
        end
      end
    end

    -- If frontmatter is found, update the 'updated' field
    if frontmatter_start and frontmatter_end then
      for i = frontmatter_start, frontmatter_end do
        if lines[i]:match("^updated%s*=%s*") then
          lines[i] = 'updated = ' .. date
          vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, lines)
          return
        end
      end
    end
  end,
})
