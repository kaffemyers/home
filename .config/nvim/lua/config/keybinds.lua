local M = {}

local utils = require("utils")
local options = { noremap = true, silent = true }

-- n - normal
-- v - visual/select
-- x - visual
-- s - select
-- t - terminal
-- i - insert
-- c - command
-- o - operator pending

function M.general_keymaps()
    return {
        -- Stop being a scrub
        {
            "<Up>",
            function()
                if vim.bo.filetype == "aerial" and vim.v.count == 0 then
                    vim.cmd("norm! k")
                end

                if vim.bo.filetype == "mason" and vim.v.count == 0 then
                    vim.cmd("norm! k")
                end

                if vim.bo.filetype == "sagafinder" and vim.v.count == 0 then
                    vim.cmd("norm! k")
                end

                if vim.v.count > 0 and vim.fn.mode() ~= "i" then
                    vim.cmd(string.format("norm! %sk", vim.v.count))
                end
            end,
            mode = { "n", "x", "i" },
            opts = options
        },
        {
            "<Down>",
            function()
                if vim.bo.filetype == "aerial" and vim.v.count == 0 then
                    vim.cmd("norm! j")
                end

                if vim.bo.filetype == "mason" and vim.v.count == 0 then
                    vim.cmd("norm! j")
                end

                if vim.bo.filetype == "sagafinder" and vim.v.count == 0 then
                    vim.cmd("norm! j")
                end

                if vim.v.count > 0 and vim.fn.mode() ~= "i" then
                    vim.cmd(string.format("norm! %sj", vim.v.count))
                end
            end,
            mode = { "n", "x", "i" },
            opts = options
        },
        {
            "<Left>",
            function()
                if vim.v.count > 0 and vim.fn.mode() ~= "i" then
                    vim.cmd(string.format("norm! %sh", vim.v.count))
                end
            end,
            mode = { "n", "x", "i" },
            opts = options
        },
        {
            "<Right>",
            function()
                if vim.v.count > 0 and vim.fn.mode() ~= "i" then
                    vim.cmd(string.format("norm! %sl", vim.v.count))
                end
            end,
            mode = { "n", "x", "i" },
            opts = options
        },

        {
            "k",
            function()
                if vim.v.count > 0 then
                    vim.cmd(string.format("norm! %sk", vim.v.count))
                end
            end,
            mode = { "n", "v" },
            opts = options
        },
        {
            "j",
            function()
                if vim.v.count > 0 then
                    vim.cmd(string.format("norm! %sj", vim.v.count))
                end
            end,
            mode = { "n", "v" },
            opts = options
        },
        {
            "h",
            function()
                if vim.v.count > 0 then
                    vim.cmd(string.format("norm! %sh", vim.v.count))
                end
            end,
            mode = { "n", "v" },
            opts = options
        },
        {
            "l",
            function()
                if vim.v.count > 0 then
                    vim.cmd(string.format("norm! %sl", vim.v.count))
                end
            end,
            mode = { "n", "v" },
            opts = options
        },

        { "<Del>",      "<NOP>",                                   mode = { "n", "v" },                                 opts = options },
        -- {
        --     "<Backspace>",
        --     function()
        --         if vim.bo.filetype == "TelescopePrompt" then
        --             vim.cmd("norm! <Backspace>")
        --         end
        --     end,
        --     mode = { "n", "v", "i" },
        --     opts = options
        -- },

        -- Remove frustrations
        { "dd",         { n = '"_dd' },                            description = "Delete - Line",                       opts = options },
        { "x",          { n = '"_x' },                             description = "Delete - Character",                  opts = options },
        { "d",          { v = '"_d' },                             description = "Delete - Selection",                  opts = options },
        { "<Del>",      { v = '"_<Del>' },                         description = "Delete - Selection",                  opts = options },
        --{ "o",          { n = "o<ESC>" },                          opts = options },
        --{ "O",          { n = "O<ESC>" },                          opts = options },

        -- Files
        { "<leader>e",  { n = "<CMD>Telescope file_browser<CR>" }, description = "File - Explore",                      opts = options },
        { "<leader>fe", { n = "<CMD>Telescope file_browser<CR>" }, description = "File - Explore",                      opts = options },
        { "<leader>ff", { n = "<CMD>Telescope find_files<CR>" },   description = "File - Find",                         opts = options },
        { "<leader>fg", { n = "<CMD>Telescope live_grep<CR>" },    description = "File - Search",                       opts = options },
        { "<leader>fc", { n = "<CMD>Telescope grep_string<CR>" },  description = "File - Search from cursor/selection", opts = options },

        -- Buffers
        { "<leader>u",  { n = "<CMD>Telescope buffers<CR>" },      description = "Buffer - Explore",                    opts = options },
        { "<leader>be", { n = "<CMD>Telescope buffers<CR>" },      description = "Buffer - Explore",                    opts = options },
        { "<leader>bq", { n = "<CMD>bd<CR>" },                     description = "Buffer - Close",                      opts = options },

        -- Zen Mode
        { "<leader>z",  { n = "<CMD>ZenMode<CR>" },                description = "Zen Mode - Toggle",                   opts = options },


        -- Terminal
        {
            "<leader>tt",
            {
                n = function()
                    local term_is_open, _ = require("toggleterm.ui").find_open_windows()
                    local zen_mode_is_open = require("zen-mode.view").is_open()

                    if zen_mode_is_open or not utils.is_floating_open() or term_is_open then
                        require("toggleterm").toggle_command("direction=float", 1)
                    end
                end,
                t = "<C-\\><C-n><CMD>ToggleTerm direction=float<CR>",
            },
            description = "Terminal Toggle",
            opts = options,
        },
        { "<ESC>",     { t = "<C-\\><C-n>" },  description = "Terminal - Normal Mode",  opts = options },

        -- Splits
        { "<C-Up>",    { n = "<C-w><Up>" },    description = "Split - Navigate Up",     opts = options },
        { "<C-Down>",  { n = "<C-w><Down>" },  description = "Split - Navigate Down",   opts = options },
        { "<C-Right>", { n = "<C-w><Right>" }, description = "Split - Navigate Right",  opts = options },
        { "<C-Left>",  { n = "<C-w><Left>" },  description = "Split - Navigate Left",   opts = options },
        { "<M-Up>",    { n = "<C-w>+" },       description = "Split - Increase Height", opts = options },
        { "<M-Down>",  { n = "<C-w>-" },       description = "Split - Decrease Height", opts = options },
        { "<M-Left>",  { n = "<C-w><" },       description = "Split - Increase Width",  opts = options },
        { "<M-Right>", { n = "<C-w>>" },       description = "Split - Decrease Width",  opts = options },

        -- Text navigation
        {
            "<leader>s",
            {
                -- n = "H^<Plug>(leap-forward-to)",
                n = "<Plug>(leap-forward-to)",
                x = "<Plug>(leap-forward-to)",
                o = "<Plug>(leap-forward-to)",
            },
            description = "Cursor - Leap Forward Inclusive",
            opts = options
        },
        {
            "<leader>S",
            {
                n = "<Plug>(leap-backward-to)",
                x = "<Plug>(leap-backward-to)",
                o = "<Plug>(leap-backward-to)",
            },
            description = "Cursor - Leap Backward Inclusive",
            opts = options,
        },
        {
            "<leader>x",
            {
                -- n = "H^<Plug>(leap-forward-till)",
                n = "<Plug>(leap-forward-till)",
                x = "<Plug>(leap-forward-till)",
                o = "<Plug>(leap-forward-till)",
            },
            description = "Cursor - Leap Forward Exclusive",
            opts = options
        },
        {
            "<leader>X",
            {
                n = "<Plug>(leap-backward-till)",
                x = "<Plug>(leap-backward-till)",
                o = "<Plug>(leap-backward-till)",
            },
            description = "Cursor - Leap Backward Exclusive",
            opts = options
        },
        {
            "<leader>sw",
            {
                n = "<Plug>(leap-from-window)",
            },
            description = "Cursor - Leap From Window",
            opts = options
        },
        {
            "<leader>sW",
            {
                n = "<Plug>(leap-cross-window)",
            },
            description = "Cursor - Leap Cross Window",
            opts = options
        },
    }
end

function M.lsp_on_attach()
    return {
        -- Code
        { "<leader>cg", { n = "<CMD>Telescope lsp_definitions<CR>" },        description = "LSP - Go To Definition",    opts = options },
        { "<leader>cfg", { n = "<CMD>Telescope lsp_definitions<CR>" },        description = "LSP - Go To Definition",    opts = options },
        { "<leader>cfi", { n = "<CMD>Telescope lsp_implementations<CR>" },        description = "LSP - Go To Implementation",    opts = options },
        { "<leader>cfr", { n = "<CMD>Telescope lsp_references<CR>" },        description = "LSP - Find References",    opts = options },
        { "<leader>ca", { n = "<CMD>lua require('actions-preview').code_actions()<CR>" }, description = "LSP - Code Action", opts = options },
        { "<leader>cf", { n = "<CMD>lua vim.lsp.buf.format()<CR>" },         description = "LSP - Format",              opts = options },
        { "<leader>cr", { n = "<CMD>Lspsaga rename<CR>" },                   description = "LSP - Rename",              opts = options },
        { "<leader>cs", { n = "<CMD>lua vim.lsp.buf.signature_help()<CR>" }, description = "LSP - Show Signature",      opts = options },
        { "<leader>ch", { n = "<CMD>lua vim.lsp.buf.hover()<CR>" },          description = "LSP - Context Float",       opts = options },
        { "<leader>cd", { n = "<CMD>Lspsaga show_cursor_diagnostics<CR>" },  description = "LSP - Diagnostics Float ",  opts = options },
        { "<leader>ce", { n = "<CMD>Telescope aerial<CR>" },                 description = "LSP - Code Outline Search", opts = options },
        { "<leader>co", { n = "<CMD>AerialToggle<CR>" },                     description = "LSP - Code Outline",        opts = options },
        {
            "<leader>cdt",
            function()
                require("lsp_lines").toggle()
                --vim.diagnostic.config({ virtual_text = not vim.diagnostic.config().virtual_text })
            end,
            mode = { "n" },
            description = "LSP - Inline Diagnostics",
            opts = options
        },

        -- Debug
        { "<leader>db", { n = "<CMD>DapToggleBreakpoint<CR>" },           description = "DAP - Toggle Breakpoint", opts = options },
        { "<leader>dc", { n = "<CMD>DapContinue<CR>" },                   description = "DAP - Continue",          opts = options },
        { "<leader>di", { n = "<CMD>DapStepInto<CR>" },                   description = "DAP - Step Into",         opts = options },
        { "<leader>do", { n = "<CMD>DapStepOver<CR>" },                   description = "DAP - Step Over",         opts = options },
        { "<leader>ds", { n = "<CMD>DapTerminate<CR>" },                  description = "DAP - Terminate",         opts = options },
        { "<leader>dl", { n = "<CMD>DapShowLog<CR>" },                    description = "DAP - Show Log",          opts = options },
        { "<leader>dt", { n = "<CMD>lua require('dapui').toggle()<CR>" }, description = "DAP - Toggle TUI",        opts = options },
    }
end

function M.lsp_on_attach_omnisharp_override()
    return {
        {
            "<leader>ca",
            function ()
                vim.lsp.buf.code_action()
            end,
            mode = { "n", "v" },
            description = "LSP - Code Action",
            opts = options

        },
        {
            "<leader>cg",
            { n = "<CMD>lua require('omnisharp_extended').telescope_lsp_definitions()<CR>" },
            description = "LSP - Go To Definition",
            opts = options
        }
    }
end

function M.completion()
    local cmp = require("cmp")
    local luasnip = require("luasnip")

    local has_words_before = function()
        if vim.api.nvim_get_option_value("buftype", { buf = 0 }) == "prompt" then return false end

        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        return col ~= 0 and vim.api.nvim_buf_get_text(0, line - 1, 0, line - 1, col, {})[1]:match("^%s*$") == nil
    end

    return {
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_locally_jumpable() then
                luasnip.expand_or_jump()
            elseif has_words_before() then
                cmp.complete()
            else
                fallback()
            end
        end, { "i", "s" }),
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end, { "i", "s" }),
        ["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
        ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
        ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
        ["<C-y>"] = cmp.config.disable, -- disable
        ["<C-e>"] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        }),
        ["<CR>"] = cmp.mapping.confirm({ select = true }),
    }
end

function M.aerial()
end

return M
